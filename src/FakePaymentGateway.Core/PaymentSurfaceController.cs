using Microsoft.AspNetCore.Mvc;
using System;
using System.Collections.Generic;
using Umbraco.Cms.Core.Cache;
using Umbraco.Cms.Core.Logging;
using Umbraco.Cms.Core.Routing;
using Umbraco.Cms.Core.Services;
using Umbraco.Cms.Core.Web;
using Umbraco.Cms.Infrastructure.Persistence;
using Umbraco.Cms.Web.Website.Controllers;

namespace FakePaymentGateway.Core
{
    public class PaymentSurfaceController : SurfaceController
    {
        private static Dictionary<string, string> PaidTransactions = new();

        private static readonly string ShopReturnUrl = "https://localhost:44333/thanks?paymentId=";
        public PaymentSurfaceController(IUmbracoContextAccessor umbracoContextAccessor,
            IUmbracoDatabaseFactory databaseFactory,
            ServiceContext services,
            AppCaches appCaches,
            IProfilingLogger profilingLogger,
            IPublishedUrlProvider publishedUrlProvider)
            : base(umbracoContextAccessor, databaseFactory, services, appCaches, profilingLogger, publishedUrlProvider)
        {
        }
        public IActionResult Pay(string transactionId)
        {
            if (!string.IsNullOrWhiteSpace(transactionId))
            {
                var paymentId = Guid.NewGuid().ToString();
                PaidTransactions.Add(transactionId, paymentId);
                return Redirect(ShopReturnUrl + paymentId);
            }
            return Content($"Invalid transactionId {transactionId}!!!");
        }

        public IActionResult Cancel() => Redirect(ShopReturnUrl);

        public static bool Verify(string transactionId, string paymentId) => PaidTransactions.TryGetValue(transactionId, out string paymentIdValue) && paymentIdValue == paymentId;

    }
}
